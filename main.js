const january = document.getElementById('jan-container');
const febuary = document.getElementById('feb-container');
const march = document.getElementById('mar-container');
const april = document.getElementById('apr-container');
const may = document.getElementById('may-container'); 
const june = document.getElementById('jun-container');
const july = document.getElementById('jul-container');
const august = document.getElementById('aug-container');
const september = document.getElementById('sep-container');
const october = document.getElementById('oct-container');
const november = document.getElementById('nov-container');
const december = document.getElementById('dec-container');
const yearElem = document.getElementById('year-mod');
const cancelBut = document.getElementById('cancel-but');
const searchBox = document.getElementById('search-div');
const searchButton = document.getElementById('search-button');
const navigation = document.getElementById('navigation');
const body = document.getElementById('main-body');
const microphoneIcon = document.getElementById('mic-icon');
const searchInput = document.getElementById('search-input');
const monthName = document.querySelectorAll('#month');

const date = new Date();
const months = ["january", "feburary", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];

var currentMonth = months[date.getMonth()];
var currentDay = date.getDate();
var currentYear = date.getFullYear();

function Create(callback) {
    var theme = "light";
    return {
        getTheme : function() { return theme; },
        setTheme : function(t) {theme = t; callback(theme); }
    };
}

var theme = Create(function(theme) {
    if (theme == "light") {
        body.classList.add("light");
    } else if (theme == "dark") {
        body.classList.remove("light");
    }
});

theme.setTheme(sessionStorage.getItem("theme"));

yearElem.innerHTML = (currentYear);

let hideSearch = () => {
    searchBox.style.visibility = "hidden";
    searchBox.classList.remove('play-leave');
}

searchButton.addEventListener('click', e => {
    searchBox.style.transform = "translateY(-120px)";
    searchBox.style.visibility = "visible";
    searchBox.classList.add('play-enter');
    setTimeout(function() {
        searchBox.style.transform = "translateY(2px)";
        searchBox.classList.remove('play-enter');
    }, 300);
});

microphoneIcon.addEventListener('click', () => {
    var speech = true;
    window.SpeechRecognition = window.webkitSpeechRecognition;
    const recognition = new SpeechRecognition();
    recognition.interimResults = true;
    recognition.continuous = true;
    recognition.addEventListener('result', e => {
        const transcript = Array.from(e.results) 
        .map(result => result[0])
        .map(result => result.transcript)
        searchInput.value = transcript;
    })
    if (speech) {
        recognition.start();
    } 
    cancelBut.addEventListener('click', e => {
        recognition.stop();
    })
})

cancelBut.addEventListener('click', e => {
    searchBox.classList.add('play-leave');
    setTimeout(hideSearch, 290);
    searchInput.value = '';
});

for (const month of monthName) {
    month.addEventListener('click', function(e) {
        window.location = "months.html";
    })
}

if (currentMonth === "january") {
    let month = january.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = january.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "febuary") {
    let month = febuary.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = febuary.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "march") {
    let month = march.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = march.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "april") {
    let month = april.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = april.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "may") {
    let month = may.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = may.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "june") {
    let month = june.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = june.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "july") {
    let month = july.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = july.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "august") {
    let month = august.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = august.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "september") {
    let month = september.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = september.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "october") {
    let month = october.querySelector('.month');
    month.style.color = '#DC524A';      
    let days = october.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else if (currentMonth === "november") {
    let month = november.querySelector('.month');
    month.style.color = '#DC524A';
    let days = november.querySelector('.month-numbers');
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "15px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
            day.style.color = "#ffffff";
        }
    }
} else {
    let month = december.querySelector('.month');
    month.style.color = '#DC524A';
    let days = december.querySelector('.month-numbers')
    for (const day of days.children) {
        if (day.innerText == currentDay) {
            if (currentDay < 10) {
                day.style.paddingLeft = "8px";
                day.style.paddingRight = "0px";
            }
            day.style.backgroundColor = '#DC524A';
            day.style.borderRadius = "20px";
        }
    }
}

navigation.addEventListener("touchstart", startTouch, false);
navigation.addEventListener("touchmove", moveTouch, false);
 
var initialX = null;
var initialY = null;
 
function startTouch(e) {
  initialX = e.touches[0].clientX;
  initialY = e.touches[0].clientY;
};
 
function moveTouch(e) {
  if (initialX === null) {
    return;
  }
 
  if (initialY === null) {
    return;
  }
 
  var currentX = e.touches[0].clientX;
  var currentY = e.touches[0].clientY;
 
  var diffX = initialX - currentX;
  var diffY = initialY - currentY;
 
  if (Math.abs(diffX) > Math.abs(diffY)) {
    if (diffX > 0) {
      console.log("swiped left");
    } else {
      console.log("swiped right");
    }  
  } else {
    if (diffY > 0) {
        setTimeout(function() {
            window.location = "home.html"; 
        }, 800);
        body.classList.add('app-exit');
    } else {
      console.log("swiped down");
    }  
  }
 
  initialX = null;
  initialY = null;
   
  e.preventDefault();
};
