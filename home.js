const dayName = document.getElementById('day-text');
const dayNumber = document.getElementById('day-num');
const modeToggle = document.getElementById('mode-icon');
const outerCircle = document.getElementById('outer');
const outerLeftSemiCircle = document.getElementById('outer-left');
const outerRightSemiCircle = document.getElementById('outer-right');
const innerRightCircle = document.getElementById('inner-right');
const innerLeftCircle = document.getElementById('inner-left');
const body = document.getElementById('main-body');

const date = new Date();
var curDayName = date.toLocaleString("default", {weekday: "short"}).toUpperCase();
var curDayNum = date.getDate();

dayName.innerHTML = (curDayName);
dayNumber.innerHTML = (curDayNum);

function Create(callback) {
    var theme = "light";
    return {
        getTheme : function() { return theme; },
        setTheme : function(t) {theme = t; callback(theme); }
    };
}

var theme = Create(function(theme) {
    if (theme == "light") {
        body.classList.add("light");
    } else if (theme == "dark") {
        body.classList.remove("light");
    }
});

theme.setTheme(sessionStorage.getItem("theme"));

modeToggle.addEventListener('click', () => {
    if (theme.getTheme() === null || theme.getTheme() === "dark") {
        theme.setTheme("light");
        sessionStorage.setItem("theme", theme.getTheme());
    } else if (theme.getTheme() === "light") {
        theme.setTheme("dark");
        sessionStorage.setItem("theme", theme.getTheme());
    } 
})



