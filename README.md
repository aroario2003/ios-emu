# ios-emu

# Introduction

This project serves as a proof of concept and a way for me to get to learn more about html, css and javascript. Also, this project is only intended to be for my own personal testing in use, so if you do want to use and try it out you are free to do so, however, beware that their maybe bugs and things may not work as expected all the time. This project attempts to recreate an iphone like design using only web development languages and is no way shape or form intended to be used like an iphone nor is it intended to be an iphone. As afforementioned, this is only a proof of concept and way for me to learn more about html, css and javascript.

## How to use

Go to the [website](https://aroario.gitlab.io/ios-emu) and go to inspect element and set the dimensions to iphone 12 pro. 
